# Moon and Crater Library

This is an Angular component library of basic, low level components that we, Moon and Crater, use throughout our projects to keep a consistent style. Components are added as we work on projects and realize the potential for that component to be used in other applications.

## Getting Started

#### Install the library:

`npm install mc-ng-lib`

#### Import the module with the components you want to use:

```typescript
import { McInputModule } from 'mc-ng-lib/input'
...

@NgModule({
  imports: [
    McInputModule
    ...
  ]
  ...
})
export class MyModule {}
```

#### Use the components within that module:

The primary component `<mc-input-wrapper>` accepts two inputs, error and valid, which accept a boolean. An error will animate an error message (if mc-input-error is inside with a message) and turn the input red. A valid input will turn the input green.

Ex:

```html
<mc-input-wrapper [error]="email.touched && email.getError('required')" [valid]="email.touched && email.valid">
  <label>Email</label>

  <input type="email" placeholder="Email" formControlName="email" />

  <mc-input-error>Email is required.</mc-input-error>
</mc-input-wrapper>
```
