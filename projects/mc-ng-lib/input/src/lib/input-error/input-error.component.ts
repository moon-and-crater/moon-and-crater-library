import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'mc-input-error',
  templateUrl: './input-error.component.html',
  styleUrls: ['./input-error.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class McInputErrorComponent {}
