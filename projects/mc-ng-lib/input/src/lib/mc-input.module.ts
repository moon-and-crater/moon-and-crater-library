import { NgModule } from '@angular/core';
import { McInputWrapperComponent } from './input-wrapper/input-wrapper.component';
import { McInputErrorComponent } from './input-error/input-error.component';

@NgModule({
  declarations: [McInputWrapperComponent, McInputErrorComponent],
  imports: [],
  exports: [McInputWrapperComponent, McInputErrorComponent]
})
export class McInputModule {}
