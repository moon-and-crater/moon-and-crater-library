import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'mc-input-wrapper',
  templateUrl: './input-wrapper.component.html',
  styleUrls: ['./input-wrapper.component.css']
})
export class McInputWrapperComponent {
  private isError = false;
  private isValid = false;

  @HostBinding('class.mc-input-error')
  @Input('error')
  get error() {
    return this.isError;
  }
  set error(val: boolean) {
    this.isError = val;
  }

  @HostBinding('class.mc-input-valid')
  @Input('valid')
  get valid() {
    return this.isValid;
  }
  set valid(val: boolean) {
    this.isValid = val;
  }
}
